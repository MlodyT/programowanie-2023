from PIL import Image
import os.path

plik = r"C:\Users\Dell\OneDrive\Pulpit\Programowanie\Lista 2\mesi.jpg"
znak_wodny = r"C:\Users\Dell\OneDrive\Pulpit\Programowanie\Lista 2\herb.png"

def add_watermark(file_to_edit, file_to_add, new_name, how_small = 6, position = (30, 30)):
    
    '''
    Tworzy i zapisuje pod podaną ścieżką, plik z podanej ścieżki, z podanym znakiem wodnym, o podanym pomniejszeniu w podanym miejscu na początkowym obrazku.

    Funkcja sprawdza czy podany plik istnieje, czy rozszerzenia pliku początkowego, znaku wodnego i nowej nazwy są rozszerzeniem dla obrazów i dla każdej niezgodnośći wypluwa odpowiedni błąd.

    Sprawdza też czy how_small to liczba większa niż 1 i czy position to dwuelementowa krotka z całkowitymi pozycjami, w przypadku problemów także wypluwa odpowiednie błędy.

    Następnie otwiera zadane pliki i przypisuje wartości rozmiaru znaku wodnego nowym zmiennym, powstałym przez how_smal - krotne pomniejszenie rozmiaru pliku, na który nakładamy znak wodny.

    Póżniej tworzy miniaturę  znaku wodnego o powyższym rozmiarze, wkleja ją na podanej pozycji na plik początkowy i zapisuje go w podanej ścieżce.

            Parametry:
                    file_to_edit (string): Ścieżka prowadzącą do pliku, do którego chcemy dodać znak wodny
                    file_to_add (string): Ścieżka prowadzącą do pliku, którego chcemy użyć jako znak wodny
                    new_name (string): Ścieżka, w której chcemy zapisać nowy plik
                    how_small (int): Liczba naturalna, która będzie dzieliła nam rozmiar pliku, do którego chcemy dodać znak wodny, aby otrzymać rozmiar znaku wodnego (domyślnie 6)
                    position (tuple): Krotka dwuelemntowa z liczbami naturalnymi, oznaczjącymi pozycję wklejenia znaku wodnego na plik początkowy (domyślnie (30,30))
            Zwrot:
                    Brak
    '''

    if os.path.isfile(file_to_edit) == True and os.path.isfile(file_to_add) == True:
        if file_to_edit.lower().endswith(('.png', '.jpg', '.jpeg')) and file_to_add.lower().endswith(('.png', '.jpg', '.jpeg')):
            if new_name.lower().endswith(('.png', '.jpg', '.jpeg')):
                if how_small > 1:
                    if type(position) == tuple and len(position) == 2:
                        if type(position[0]) == int and type(position[1]) == int and position[0] > 0 and position[1] > 0:
                            watermark = Image.open(file_to_add)
                            zdjęcie = Image.open(file_to_edit)
                            x, y = zdjęcie.size
                            rozmiar = int(x/how_small), int(y/how_small)

                            watermark.thumbnail(rozmiar)

                            converted_watermark = watermark.convert("RGBA")
                            converted_watermark.putalpha(127)

                            zdjęcie.paste(converted_watermark, position, converted_watermark)
                            zdjęcie = zdjęcie.save(new_name)
                        else:
                            raise ValueError("Wartości wymiarów powinny być naturalne")
                    else:
                        raise TypeError("Pozycja powinna być krotką z dwoma wymiarami chcianej miniatury")
                else:
                    raise ValueError("Ten parametr powinien być liczbą większą niż")                     
            else:
                raise ValueError("Rozszerzenie nowego pliku nie jest rozszerzeniem dla obrazów")
        else:
            raise ValueError("Plik z obrazem, na który ma zostac doany znak wodny albo plik ze znakiem wodny nie jest obrazem")
    else:
        raise FileExistsError("Plik z obrazem, na który ma zostac doany znak wodny albo plik ze znakiem wodny nie istnieje")

add_watermark(plik, znak_wodny, r"C:\Users\Dell\OneDrive\Pulpit\zdjęcie_ze_znakiem.jpg")