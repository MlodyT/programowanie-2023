from PIL import Image
import os.path

plik = r"C:\Users\Dell\OneDrive\Pulpit\Programowanie\Lista 2\mesi.jpg"
rozmiar = 500, 500
nowa_nazwa = r"C:\Users\Dell\OneDrive\Pulpit\nowymesi.jpg"

def miniatura(file, size, name):

    '''
    Tworzy i zapisuje pod podaną ścieżką, miniaturę pilku o podanej ścieżce, zmieniając jego wymiary na podane wymiary.

    Funkcja sprawdza czy podany plik istnieje, czy rozszerzenie pliku, którego miniaturę tworzymy i nowej nazwy są rozszerzeniem dla obrazów i dla każdej niezgodnośći wypluwa odpowiedni błąd.

    Następnie otwiera zadany plik i dodatkowo sprawdza czy nowy rozmiar jest dwuelementową krotką z liczbami całkowitymi, gdy także tutaj coś się nie zgadza wypluwany jest odpowiedni błąd.

    Gdy wszystko się zgadza funkcja zmienia wymiary zdjęcia na podane i zapisuje nowy obraz pod podaną ścieżką.

            Parametry:
                    file (string): Ścieżka prowadzącą do pliku, którego chcemy otrzymać miniaturę, z odpowiednim rozszerzeniem
                    size (tuple): Krotka dwuelementowa z wymiarami miniatury w postaci liczb naturalnych
                    name (string): Ścieżka prowadzącą do miejsca, gdzie chcemy zapisać miniature, konćząca się nową nazwą z odpowiednim rozszerzeniem

            Zwrot:
                    Brak
    '''
        
    if os.path.isfile(file) == True:
        if file.lower().endswith(('.png', '.jpg', '.jpeg')):
            if name.lower().endswith(('.png', '.jpg', '.jpeg')):
                with Image.open(file) as im:
                    if type(size) == tuple and len(size) == 2:
                        if type(size[0]) == int and type(size[1]) == int:
                            if size[0] > 0 and size[1] > 0:
                                zdjęcie = im.resize(size)
                                zdjęcie = zdjęcie.save(name)
                            else:
                                raise ValueError("Wartości wymiarów powinny być dodatnie")
                        else:
                            raise TypeError("Podane w krotce wartości to nie liczby całkowite")
                    else:
                        raise TypeError("Rozmiar powinien być krotką z dwoma wymiarami chcianej miniatury")
            else:
                raise ValueError("Rozszerzenie nowego pliku nie jest rozszerzeniem dla obrazów")
        else:
            raise ValueError("Ten plik nie jest obrazem")
    else:
        raise FileExistsError("Ten plik nie istnieje")

miniatura(plik, rozmiar, nowa_nazwa)