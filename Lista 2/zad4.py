from PyPDF2 import PdfReader, PdfWriter
import math
import os.path

nazwa = r"C:\Users\Dell\OneDrive\Pulpit\Programowanie\Lista 2\Struktura_sadownictwa_w_Polsce.pdf"

def split_pdf_how_many_pages(plik, ile_stron):

    '''
    Funkcja sprawdza najpierw czy podany plik istnieje, czy jest plikiem pdf i czy liczba stron w jednym pliku jest liczbą całkowitą, gdy pojawia się problem wypluwa odpowiedni błąd.

    Jeżeli nie ma problemów funkcja otwiera nasz plik i następnie tworzy tyle plików ile wynika z tego ile stron starego pliku ma zawierać każdy jeden nowy plik.

    Później tworzy i zapisuje po kolei pliki z podaną ilością stron, o nazwach jak ten początkowy, tylko, że z kolejnymi numerami jako przyrostek.

    W przypadku, gdy funkcja dochodzi do tworzenia ostatniego pliku, może się zdarzyć, że "nie starczy" jej stron, żeby ten plik miał tyle sam stron, co poprzednie, wtedy będzie on zawierał po prostu pozostałe strony.

            Parametry:
                    plik (string): Ścieżka prowadzącą do pliku pdf, który chcemy podzielić
                    ile_stron (int): Liczba naturalna ilości stron, któr mają zawierać nowe pliki

            Zwrot:
                    Brak
    '''

    if os.path.isfile(plik) == True:
        if plik.lower().endswith((".pdf")):
            if ile_stron > 0 and type(ile_stron) == int:
                file = PdfReader(plik)
                ktora_strona = 0
                result = os.path.splitext(plik)[0]

                for i in range(math.ceil(len(file.pages)/ile_stron)):
                    output = PdfWriter()
                    if len(file.pages) - ktora_strona > ile_stron:
                        for j in range(ile_stron):
                            output.add_page(file.pages[ktora_strona])
                            ktora_strona = ktora_strona + 1
                    else:
                        for j in range(len(file.pages) - ktora_strona):
                            output.add_page(file.pages[ktora_strona])
                            ktora_strona = ktora_strona + 1
                    with open(result + "_" + str(i + 1) + ".pdf", "wb") as nazwa:
                        output.write(nazwa)
            else:
                raise ValueError("Podana ilość stron nie jest liczbą naturalną")
        else:
            raise ValueError("Podany plik nie jest pdf'em")
    else:
        raise FileExistsError("Podany plik nie istnieje")

def split_pdf_how_many_files(plik, ile_plików):

    '''
    Funkcja sprawdza najpierw czy podany plik istnieje, czy jest plikiem pdf i czy liczba plików pdf do stworzenia jest liczbą całkowitą, gdy pojawia się problem wypluwa odpowiedni błąd.

    Jeżeli nie ma problemów funkcja otwiera nasz plik i następnie tworzy tyle plików ile podaliśmy

    Później tworzy i zapisuje po kolei pliki z wyliczoną ilością stron, o nazwach jak ten początkowy, tylko, że z kolejnymi numerami jako przyrostek.

    W przypadku, gdy funkcja dochodzi do tworzenia ostatniego pliku, może się zdarzyć, że "nie starczy" jej stron, żeby ten plik miał tyle sam stron, co poprzednie, wtedy będzie on zawierał po prostu pozostałe strony.

            Parametry:
                    plik (string): Ścieżka prowadzącą do pliku pdf, który chcemy podzielić
                    ile_plików (int): Liczba naturalna ilości plików, które mają zostać stworzone

            Zwrot:
                    Brak
    '''
    
    if os.path.isfile(plik) == True:
        if plik.lower().endswith((".pdf")):
            if ile_plików > 0 and type(ile_plików) == int:
                file = PdfReader(plik)
                ktora_strona = 0
                result = os.path.splitext(plik)[0]
                
                for i in range(ile_plików):
                    output = PdfWriter()
                    if len(file.pages) - ktora_strona > math.ceil(len(file.pages)/ile_plików):
                        for j in range(math.ceil(len(file.pages)/ile_plików)):
                            output.add_page(file.pages[ktora_strona])
                            ktora_strona = ktora_strona + 1
                    else:
                        for j in range(len(file.pages) - ktora_strona):
                            output.add_page(file.pages[ktora_strona])
                            ktora_strona = ktora_strona + 1
                    with open(result + "_" + str(i + 1) + ".pdf", "wb") as nazwa:
                        output.write(nazwa)
            else:
                raise ValueError("Podana ilość plików pdf nie jest liczbą naturalną")
        else:
            raise ValueError("Podany plik nie jest pdf'em")
    else:
        raise FileExistsError("Podany plik nie istnieje")
    
def split_pdf_which_pages(plik, strony):

    '''
    Funkcja sprawdza najpierw czy podany plik istnieje, czy jest plikiem pdf i czy strony, na których ma zostać podzielony pdf to liczby naturalne w liście, gdy pojawia się problem wypluwa odpowiedni błąd.

    Jeżeli nie ma probelmów, to funkcja sortuje naszą listę ze stronami rosnąco, a następnie otwiera nasz plik pdf, potem sprawdza jeszcze czy któraś ze stron, na której ma być podział nie wykracza poza zasię gstron pliku pdf.

    Później tworzy tyle n + 1 nowych plików pdf, gdzie n to długość listy ze stronami, o nazwach takich jak plik początkowy, tylko z kolejnymi cyframi jako przyrostki.

    Przy pomocy zmiennej monitoruje ile plików zostało już utworzonych i zależnie od tego podejmuje odpowiednie działanie.

    Do pierwszego pliku dodaje wszystkie storny od początku do pierwszej strony, na której ma wystąpić podział.

    Dla "środkowych" plików dodaje strony od poprzedniej strony, na której był podział do aktualnej strony, na której ma być podział

    Dla ostatniego pliku dodaje wszystkie pliki od ostatniej strony, na której miał być podział do ostatniej strony pdf'a.

            Parametry:
                    plik (string): Ścieżka prowadzącą do pliku pdf, który chcemy podzielić
                    strony (int): Liczba naturalna ilości plików, które mają zostać stworzone

            Zwrot:
                    Brak
    '''
        
    if os.path.isfile(plik) == True:
        if plik.lower().endswith((".pdf")):
            if type(strony) == list:
                test = 0

                file = PdfReader(plik)

                
                for i in strony:
                    if type(i) == int and i > 0:
                        test = test + 1
                    
                    if i > len(file.pages):
                        raise ValueError("Ten plik nie ma tylu stron")
                        break
                
                strony.sort()
                
                if test == len(strony):
                    result = os.path.splitext(plik)[0]

                    ile_razy = 0

                    if strony[-1] != len(file.pages):         
                        for i in range(len(strony) + 1):
                            output = PdfWriter()
                                        
                            if ile_razy == 0:
                                for j in range(strony[i]):
                                    output.add_page(file.pages[j])

                            elif 0 < ile_razy < len(strony):
                                for j in range(strony[i-1], strony[i]):
                                        output.add_page(file.pages[j])

                            else:
                                for j in range(strony[-1], len(file.pages)):
                                    output.add_page(file.pages[j])
                            
                            with open(result + "_" + str(i + 1) + ".pdf", "wb") as nazwa:
                                output.write(nazwa)
                                            
                            ile_razy = ile_razy + 1
                    
                    else:
                        for i in range(len(strony)):
                            output = PdfWriter()
                                        
                            if ile_razy == 0:
                                for j in range(strony[i]):
                                    output.add_page(file.pages[j])

                            elif 0 < ile_razy < len(strony):
                                for j in range(strony[i-1], strony[i]):
                                        output.add_page(file.pages[j])
                            
                            with open(result + "_" + str(i + 1) + ".pdf", "wb") as nazwa:
                                output.write(nazwa)
                                            
                            ile_razy = ile_razy + 1


                else:
                    raise TypeError("Nie wszystkie wartości podane w liście, to liczby naturalne")
            else:
                raise ValueError("Podany parametr nie jest listą")
        else:
            raise ValueError("Podany plik nie jest pdf'em")
    else:
        raise FileExistsError("Podany plik nie istnieje")

split_pdf_which_pages(nazwa, [3, 9, 21])