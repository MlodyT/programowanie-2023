import string
import numpy as np

elementy = string.ascii_letters + string.digits + string.punctuation

def generate(n):
    
    '''
    Zwraca hasło o zadanej przez parametr długośći.

    Funkcja sprawdza czy podana wartość jest liczbą naturalną, gdy tak nie jest wypluwa błąd.

    Następnie tworzy hasło składające się z losowo wybranych małych i dużych liter, cyfr oraz znaków o zadanej przez nas długości.

            Parametry:
                    n (int): Dodatnia liczba informująca o długości hasła jakie chcemy uzyskać

            Zwrot:
                    hasło (string): Tekst składający się z losowych znaków, cyfr i liter
    '''

    if n > 0 and type(n) == int:
        hasło = ""
        for i in range (n):
            hasło += elementy[np.random.randint(0, len(elementy))]
        return hasło
    else:
        raise TypeError("Podana dlugość hasła nie jest liczbą naturalną")

def check_number(text):

    '''
    Sprawdza czy podany tekst zawiera co najmniej po jednej literze, cyfrze i znaku specjalnym i w zależności od tego zwraca wartość logiczną.

    Funkcja tworzy trzy zmienne odpowiadające każdej z grup: litery, znaki specjalne i cyfry, i do odpowiadającej danemu znakowi dodaje 1, gdy występuje on w tekście.

    Następnie sprawdza czy na pewno wszystkie zmienne są większ niż zero, gdy tak jest zwraca True, gdy nie False.

            Parametry:
                    text (string): Tekst, który chcemy analizować pod względem zawartośći znaków specjalnych, cyfr i liter

            Zwrot:
                    True albo False (bool): Wartość logiczna, zależnie od tego czy tekst zawiera znak specjalny, liczbę i literę
    '''

    litery = 0
    liczby = 0
    znaki = 0
    for i in text:
        if i in string.ascii_letters:
            litery = litery + 1
        if i in string.digits:
            liczby = liczby + 1
        if i in string.punctuation:
            znaki = znaki + 1
    if litery == 0 or liczby == 0 or znaki == 0:
        return False
    else:
        return True

password = generate(8)

while check_number(password) == False:
    password = generate(len(password))
else:
    print(password)