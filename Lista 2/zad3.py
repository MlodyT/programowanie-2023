from zipfile import ZipFile
import zipfile
import os.path
from datetime import date

def backup_copy(foldery):

    '''
    Funkcja sprawdza czy podany parametr jest listą ze ścieżkami do folderów.

    Dla każdej ścieżki w liście funkcja najpierw tworzy skompresowany folder o nazwie kompresowanego folderu z przedrostkiem w postaci daty i przyrostkiem w postaci kopia.

    Następnie dla każdego pliku zapisuje jego kopię w skompresowanym folderze w takim samym miejscu jak był w orginalnym folderze.

            Parametry:
                    foldery (list): Lista ze ścieżkami do folderów, które chcemy skompresować

            Zwrot:
                    Brak
    '''

    if type(foldery) == list:
        for i in range(len(foldery)):
            if os.path.isdir(foldery[i]) == True:
                with ZipFile(str(date.today()) + " " + os.path.basename(foldery[i]) + "kopia.zip", "w", zipfile.ZIP_DEFLATED) as zip:
                    for nazwa_folderu, podfolder, nazwy_plików in os.walk(foldery[i]):
                        for i in nazwy_plików:
                            ścieżka = os.path.join(nazwa_folderu, i)
                            zip.write(ścieżka, os.path.basename(ścieżka), compress_type = zipfile.ZIP_DEFLATED, compresslevel = 9)
            else:
                raise FileExistsError("Jeden z podanych folderów nie istnieje")
    else:
        raise TypeError("Argumentem tej funkcji powinna być lista ze ścieżkami do folderów")

lista = [r"C:\Users\Dell\OneDrive\Pulpit\Programowanie\Lista 2\test", r"C:\Users\Dell\OneDrive\Pulpit\Programowanie\Lista 2\Karty"]

backup_copy(lista)