import string

działanie = "9999+999"

def słupek(tekst, czy_wynik = True):
    
    '''
    Funkcja sprawdza najpierw czy podane parametry to koleno string i bool, a potem przy pomocy zmiennej czy nas string zawiera wyłącznie liczby i dokładnie jeden z dozwolonych operatorów.

    Gdy pojawią się problemy wypluwane są odpowiednie błędy, gdy nie ma problemów funkcja "wyciąga" z naszego stringa liczby i operator działania.

    Póżniej zależnie od operatora wykonuje odpowiednie obliczenia i wylicza wynik.

    Następnie zależnie od tego ile cyfr składa się pierwsza i druga liczba oraz wynik, printuje odpowiedni słupek z działaniem.

    Dodatkowo zależenie od wartości logicznej drugiego parametru printuje lub nie, również wynik.

            Parametry:
                    tekst (string): Tekst z działaniem, z którego mamy stworzyć słupek
                    czy_wynik (bool): Wartość logiczna True, gdy chcemy, żeby wyprintowało nam wynik, a False, gdy nie (domyślknie True)

            Zwrot:
                    Brak
    '''
    
    if type(tekst) == str:
        if type(czy_wynik) == bool:
            sprawdzenie = 0
            test = 0

            dozwolone_obliczenia = ["+", "-", "*"]
            dozwolone_znaki = str(dozwolone_obliczenia) + string.digits

            for i in tekst:
                if i in dozwolone_obliczenia:
                    indeks_znaku = tekst.index(i)
                    znak = i
                    test = test + 1
                if i in dozwolone_znaki:
                    sprawdzenie = sprawdzenie + 1
            
            if sprawdzenie == len(tekst) and test == 1:
                pierwsza_liczba = int(tekst[:indeks_znaku])
                druga_liczba = int(tekst[(indeks_znaku + 1):])

                if znak == "+":
                    wynik = pierwsza_liczba + druga_liczba
                if znak == "-":
                    wynik = pierwsza_liczba - druga_liczba
                if znak == "*":
                    wynik = pierwsza_liczba * druga_liczba

                if len(str(pierwsza_liczba)) >= len(str(druga_liczba)):
                    if len(str(wynik)) - len(str(pierwsza_liczba)) <= 2:
                        print(" " * 2 + str(pierwsza_liczba))
                        print(znak + " " * (len(str(pierwsza_liczba)) - len(str(druga_liczba)) + 1) + str(druga_liczba))
                        print("-" * (len(str(pierwsza_liczba)) + 2))
                        if czy_wynik == True:
                            print(" " * (len(str(pierwsza_liczba)) + 2 - len(str(wynik))) + str(wynik))
                    else:
                        print(" " * (len(str(wynik)) - len(str(pierwsza_liczba))) + str(pierwsza_liczba))
                        print(znak + " " * (len(str(wynik)) - len(str(druga_liczba)) - 1) + str(druga_liczba))
                        print("-" * (len(str(wynik))))
                        if czy_wynik == True:
                            print(str(wynik))
                if len(str(pierwsza_liczba)) < len(str(druga_liczba)):
                    if len(str(wynik)) - len(str(druga_liczba)) <= 2:
                        print(" " * (len(str(druga_liczba)) - len(str(pierwsza_liczba)) + 2) + str(pierwsza_liczba))
                        print(znak + " " + str(druga_liczba))
                        print("-" * (len(str(druga_liczba)) + 2))
                        if czy_wynik == True:
                            print(" " * (len(str(druga_liczba)) + 2 - len(str(wynik))) + str(wynik))
                    else:
                        print(" " * (len(str(wynik)) - len(str(pierwsza_liczba))) + str(pierwsza_liczba))
                        print(znak + " " * (len(str(wynik)) - len(str(druga_liczba)) - 1) + str(druga_liczba))
                        print("-" * (len(str(wynik))))
                        if czy_wynik == True:
                            print(str(wynik))
            else:
                raise TypeError("Tekst z działaniem może zawierać tylko cyfyry i dokładnie jeden z podanych tu operatorów: %s" % (dozwolone_obliczenia))
        else:
            raise TypeError("Drugi parametr może wyłacznie być wartość logiczną, True lub ewentualnie pominięty gdy chcemy wynik, False, gdy tego wyniku nie chcemy")
    else:
        raise TypeError("Działanie powinno być podane jakos string")

słupek(działanie)