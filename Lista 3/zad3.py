from PyPDF2 import PdfMerger, PdfReader
import os.path

def merging(files, new_name):
    '''
    Łączy podane pliki pdf w jeden duży i zapisuje je pod podaną nazwą.
    
    Funkcja sprawdza czy pierwszy argument to lista ze ścieżkami do plików, które istnieją i są formatu pdf.

    Potem sprawdza czy ścieżka, którą mamy nadać połączonemu plikowi kończy się rozszerzeniem pdf.

    Gdy wszystko się zgadza tworzy nowy plik, do którego otweira po kolei pliki z poadnej wcześniej listy i dodaje je do nowego pliku.

    Na końcu zapisuje duży plik pod podaną nazwą.

            Parametry:
                    files (list): Lista ze ścieżkami do plików pdf, które mają zostać połączone
                    new_name (string): Ścieżka do nazwy pliku, pod którą ma zostać zapisany plik z połączonymi pdf'ami

            Zwrot:
                    Brak
    '''
    if type(files) == list:
        for i in files:
            if os.path.isfile(i) == True and i.lower().endswith(('.pdf')):
                if new_name.lower().endswith(('.pdf')):
                    duży_plik = PdfMerger()

                    for i in range(len(files)):
                        file = PdfReader(files[i])
                        duży_plik.append(file)

                    duży_plik.write(new_name)
                else:
                    raise TypeError("Rozszerzenie w przypadku nowej nazwy nie jest rozszerzeniem pdf")
            else:
                raise TypeError("Któryś z podanych plików nie jest pdf'em lub nie istnieje")
    else:
        raise TypeError("Podany argument nie jest listą ze ścieżkami do plików pdf")
    
pliki = [r"C:\Users\Dell\Downloads\TERMINARZ.pdf", r"C:\Users\Dell\Downloads\int1329.pdf", r"C:\Users\Dell\Downloads\sprawdzian_1__zakres_rozszerzony_2022.pdf"]

merging(pliki, r"C:\Users\Dell\OneDrive\Pulpit\polaczony.pdf")