import os.path

def endline(file, windows_to_unix = True):
    '''
    Funkcja sprawdza czy podany plik istnieje i ma rozszerzenie txt, a czy drugi parmetr to True lub False.

    Potem otwiera ten plik binarnie, przypisuje jego treść zmiennej i zamienia znaki końca linii, a następnie zapisuje ten plik ze zmienionym tekstem.

            Parametry:
                    file (string): Ścieżka do pliku, w którym mają zostać zmienione znaki końca linii 
                    windows_to_unix (bool): Wartość logiczna, zależnie od tego czy chcemy zmieniać znaki z windowsa na unixa czy odwrotnie(domyślnie True)

            Zwrot:
                    Brak
    '''
    if os.path.isfile(file) == True and file.lower().endswith(('.txt')):
        if type(windows_to_unix) == bool:
            if windows_to_unix == True:
                with open (file, "rb") as plik:
                    tekst = plik.read
                    nowy_tekst = tekst().replace(b"\r\n", b"\n")
                    plik.close()
                    
                with open (file, "wb") as plik:
                    plik.write(nowy_tekst)
                    plik.close()

            elif windows_to_unix == False:
                with open (file, "rb") as plik:
                    tekst = plik.read
                    nowy_tekst = tekst().replace(b"\n", b"\r\n")
                    plik.close()
                    
                with open (file, "wb") as plik:
                    plik.write(nowy_tekst)
                    plik.close()
        else:
            raise TypeError("Drugi argument nie jest wartością logiczną")
    else:
        raise TypeError("Któryś z podanych plików nie istnieje lub nie jest plikiem txt")

endline("tekst.txt", False)