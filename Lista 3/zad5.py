nawiasy = ["(", ")", "[", "]", "{", "}", "<", ">"]

def check(text):
    '''
    Sprawdza czy w podanym tekście prawidłowo wpisano nawiasy.

    Funkcja tworzy zmienną, do której dodaje tylko nawiasy z podanego tekstu, sprawdza czy ilość nawiasów jest parzysta, jeśli nie jest wyrzuca False.

    Potem analizując powstały tekst, gdy napotka w nim otwarcie nawiasu dodaje je do utworzonej wcześniej listy, a gdy napotka zamknięcie.

    Wtedy, gdy ostatni element listy to odpowiadający owtierający, usuwa ostatnią pozycję listy, gdy nawiasy do siebie nie pasują wypluwa False.

    Robi tak do momentu aż przeanalizuje cały tekst, a potem jeszcze sprawdza czy ilość otwarć i zamknięć nawiasówjest taka sama, gdy nie jest wypluwa False.

    W przypadku pomyślnego przejścia wszystkich prób wypluwa True.

            Parametry:
                    text (string): Tekst, który chcemy analizować pod względem poprawności nawiasów

            Zwrot:
                    True albo False (bool): Wartość logiczna, zależnie od tego czy nawiasy w tekście są ustawione prawidłowo
    '''

    if type(text) == str:    
        
        new_string = ""
        tablica = []

        for i in text:
            if i in nawiasy:
                new_string = new_string + i

        if len(new_string) % 2 == 1:
            return False
        
        nawiasy_pocz = nawiasy[::2]
        nawiasy_kon = nawiasy[1::2]

        otwarcia = 0
        zamknięcia = 0

        for i in text:
            if i in nawiasy_pocz:
                tablica.append(i)
                otwarcia = otwarcia + 1
            if i in nawiasy_kon:
                zamknięcia = zamknięcia + 1

                if nawiasy_pocz.index(tablica[-1]) != nawiasy_kon.index(i):
                    return False
                else:
                    del tablica [-1]
        
        if otwarcia != zamknięcia:
            return False
        else:
            return True
    
    else:
        raise TypeError("Podana wartość to nie string")

tekst = "{[(1-9+1)]*4+(10-1)}"

print(check(tekst))