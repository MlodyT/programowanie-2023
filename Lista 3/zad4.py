import qrcode
import cv2
import os.path

def make_qr(info, name):
    '''
    Genruje kod QR przechowujący podaną informację i zapisuje go pod podaną nazwą.
    
    Funkcja sprawdza czy podana ścieżka, pod którą ma zostać zapisany kod kończy się rozszerzeniem dla obrazów.

    Potem sprawdza czy informacja, którą mamy "zakodować" jest stringiem.

    Gdy wszystko się zgadza tworzy kod QR z podaną informacją i zapisuje go pod podaną nazwą.

            Parametry:
                    info (string): Tekst, który ma być przechowywany przez kod QR
                    name (string): Ścieżka do nazwy pliku, pod którą ma zostać zapisany plik z kodem QR

            Zwrot:
                    Brak
    '''

    if name.lower().endswith(('.png', '.jpg', '.jpeg')):
        if type(info) == str:
            image = qrcode.make(info)

            image.save(name)
        else:
            raise TypeError("Podana wartość do zakodowania nie jest stringiem")
    else:
        raise ValueError("Podana nazwa, pod którą ma zostać zapisany plik nie ma rozszerzenia dla obrazów")

def decode_qr(name):
    '''
    Odczytuje informację jaką przechowuje kod QR i printuje ją.
    
    Funkcja sprawdza czy podany plik istnieje i jest obrazem, gdy tak jest otweira ten obraz i sprawdz czy da się na nim zidentyfikować kod QR.

    Gdy identyfikacja jest możliwa odczytuje informację i ją printuje.

            Parametry:
                    name (string): Ścieżka do nazwy pliku, pod którą ma zostać zapisany plik z kodem QR

            Zwrot:
                    Brak
    '''

    if os.path.isfile(name) == True:
        if name.lower().endswith(('.png', '.jpg', '.jpeg')):
            code = cv2.imread(name)
            
            if cv2.QRCodeDetector().detectAndDecodeMulti(code)[0] == False:
                raise ValueError("Podany obraz nie zawiera możliwego do zidentyfikowania kodu qr")
            else:
                print(cv2.QRCodeDetector().detectAndDecodeMulti(code)[1][0])
        else:
            raise ValueError("Podany plik do odkodowania nie jest obrazem")
    else:
        raise FileExistsError("Podany plik do odkodowania nie istnieje")

plik = r"C:\Users\Dell\OneDrive\Pulpit\kod1.png"

make_qr("tajna info", plik)

decode_qr(plik)