import requests
from bs4 import BeautifulSoup
import webbrowser

while True:
    link = requests.get("https://pl.wikipedia.org/wiki/Specjalna:Losowa_strona")
    strona = BeautifulSoup(link.content, 'html.parser')
    tytuł = strona.title.string
    przyrostek = " – Wikipedia, wolna encyklopedia"
    nazwa = tytuł.split(przyrostek)
    print (nazwa[0])

    odpowiedź = input("Napisz tak jak chcesz otworzyć ten artykuł \n")

    if odpowiedź == "tak":
        webbrowser.open("https://pl.wikipedia.org/wiki/" + nazwa[0])
        break
    else:
        continue