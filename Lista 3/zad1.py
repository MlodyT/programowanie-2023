import os
import shutil
from datetime import date

def backup_copy(where_to_look, extensions, where_to_save = r"C:\Users\Dell\OneDrive\Pulpit"):
    '''
    Tworzy w podanym miejscu folder Backup, o ile wcześniej nie istniał i zapisuje tam foldery-kopie zapasowe plików z podanych do funkcji katalogów, o podanych rozszerzeniach, edytyowanych co najwyżej 3 dni temu, w podanej ścieżce.

    Funkcja sprawdza czy dwa pierwsze argumenty to odpowiednio lista i krotka, mają one zawierać foldery do sprawdzenia i rozszerzenia plików do zapisania.

    Następnie sprawdza jeszcze czy ścieżka do zapiszania  folderu Backup istnieje, gdy coś się nie zgadza wyświetla odpowiednie błędy.

    Później sprawdza czy w podanej lokalizacji istnieje folder Backup, jeżeli nie tworzy go tam.

    Potem dla każdego folderu, który mamy sprawdza czy on istnieje, gdy tak jest tworzy folder copy z dzisiejszą datą w katalogu Backup.

    Później zapisuje do niego wszystkie pliki o podanych rozszerzeniach, modyfikowane w przeciągu ostatnich trzech dni.

            Parametry:
                    where_to_look (list): Lista ze ścieżkami do folderów, które mają zostać przeszukane
                    extensions (tuple): Krotka z rozszerzeniami, które mają zostać zapisane do kopii zapasowej
                    where_to_save (string): Ścieżka do folderu, w którym ma powstać folder Backup

            Zwrot:
                    Brak
    '''
    
    if type(where_to_look) == list:
        if type(extensions) == tuple:
            if os.path.isdir(where_to_save) == True:
                folder_backup = os.path.join(where_to_save, "Backup")

                if os.path.isdir(folder_backup) == False:
                        os.mkdir(folder_backup)

                for i in range(len(where_to_look)):
                    ścieżka = os.path.join(where_to_save, "Backup\copy-" + os.path.basename(where_to_look[i]) + "-" + str(date.today()))

                    if os.path.isdir(where_to_look[i]) == True:

                        if os.path.isdir(ścieżka) == False:
                            os.mkdir(ścieżka)

                        for root, directories, files in os.walk(where_to_look[i]):
                            for filename in files:
                                if filename.lower().endswith((extensions)):
                                        
                                    modyfikacja = date.today() - date.fromtimestamp(os.path.getmtime(os.path.join(root, filename)))

                                    if modyfikacja.days <= 3:
                                        shutil.copy(os.path.join(root, filename), ścieżka)
                    
                    else:
                        raise FileExistsError("Jeden z podanych folderów nie istnieje")
            else:
                raise FileExistsError("Podany do zapisania folder nie istnieje")
        else:
            raise TypeError("Drugi element nie jest krotką z roszerzeniami")
    else:
        raise TypeError("Pierwszy argument nie jest listą ze ścieżkami prowadzącymi do folderów")
    
gdzie = [r"C:\Users\Dell\Downloads", r"C:\Users\Dell\OneDrive\Pulpit\test", r"C:\Users\Dell\OneDrive\Pulpit\Wstęp do programowania"]
rozszerzenia = (".py", ".txt", ".jpg", ".pdf")
            
backup_copy(gdzie, rozszerzenia)