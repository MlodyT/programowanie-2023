import requests
import os.path
import json
from tkinter import *
from tkinter import ttk
import tkinter as tk
import sys

try:
    waluty_json = requests.get("http://api.nbp.pl/api/exchangerates/tables/A/?format=json")
    
    if os.path.isfile("waluty.json") == False:
        file = open("waluty.json", "x")

    with open("waluty.json", "wb") as file:
        file.write(waluty_json.content)

    złoto_json = requests.get("http://api.nbp.pl/api/cenyzlota/?format=json")

    if os.path.isfile("złoto.json") == False:
        file = open("złoto.json", "x")

    with open("złoto.json", "wb") as file:
        file.write(złoto_json.content)
    
    internet = True
except requests.exceptions.RequestException:
    internet = False

with open("waluty.json", "rb") as file:
    waluty_data = json.load(file)[0]

data_walut = waluty_data["effectiveDate"]
kursy_walut = waluty_data["rates"]

nazwy_walut = ["złoty"]
kody_walut = ["PLN"]
przeliczniki_walut = [1]

for waluta in kursy_walut:
    nazwy_walut.append(waluta["currency"])
    kody_walut.append(waluta["code"])
    przeliczniki_walut.append(waluta["mid"])

pelne_nazwy = []

for i in range(len(nazwy_walut)):
    pelne_nazwy.append(nazwy_walut[i] + " (" + kody_walut[i] + ")")

with open("złoto.json", "rb") as file:
    złoto_data = json.load(file)[0]

data_złota = złoto_data["data"]
kurs_złota = złoto_data["cena"]



def przeliczanie(kod1, kod2, kwota):
    '''
    Funkcja przelicza podaną kwotę w jednej walucie, na drugą walutę na podstawie podanych kodów walut.
    
    Na początku sprawdza, czy któraś z podanych walut jest walutą polską, jeżeli tak, to oszczędzając czas wykonuje prostsze obliczenia, aby otrzymać ostateczną kwotę.

    Gdy waluty są inne liczy kwotę ze wzoru, a na końcu zaokrągla ją do dwóch cyfr po przecinku.

            Parametry:
                    kod1 (string): Kod waluty, z której przeliczamy naszą kwotę
                    kod2 (string): Kod waluty, na którą przeliczamy naszą kwotę
                    kwota (int): Ilość pieniędzy, które będziemy przeliczać z pierwszej na drugą walutę

            Zwrot:
                    ilość_końcowa (float): Ilość pieniędzy obliczona w danej walucie
    '''

    if kod1 == "PLN":
        indeks = kody_walut.index(kod2)
        przelicznik = przeliczniki_walut[indeks]
        ilość_końcowa = round(kwota/przelicznik, 2)
    elif kod2 == "PLN":
        indeks = kody_walut.index(kod1)
        przelicznik = przeliczniki_walut[indeks]
        ilość_końcowa = round(kwota*przelicznik, 2)
    else:
        indeks1 = kody_walut.index(kod1)
        indeks2 = kody_walut.index(kod2)
        przelicznik1 = przeliczniki_walut[indeks1]
        przelicznik2 = przeliczniki_walut[indeks2]
        ilość_końcowa = round((kwota*przelicznik1)/przelicznik2, 2)
    return ilość_końcowa

def złoto_inna_waluta(kod):
    '''
    Funkcja przelicza cenę złota za 1g ze złotych na podaną walutę, na podstawie jej podanego kodu.

            Parametry:
                    kod (string): Kod waluty, na którą przeliczamy naszą cenę złota

            Zwrot:
                    kurs (float): Kurs złota w innej walucie
    '''

    indeks = kody_walut.index(kod)
    przelicznik = przeliczniki_walut[indeks]
    kurs = kurs_złota/przelicznik
    return kurs

def złoto_w_gramach(kod, masa):
    '''
    Funkcja przelicza podaną masę złota w gramavh, na podaną walutę.
    
    Najpierw liczy cenę złota w podanej walucie na podstawie funkcji wyżej, a potem mnoży tę kwotę przez podaną masę

            Parametry:
                    kod (string): Kod waluty, na którą przeliczamy naszą ilośc złota
                    masa (float): Masa złota
            Zwrot:
                    kwota (float): Cena podanej masy złota, w podanej walucie
    '''

    przelicznik = złoto_inna_waluta(kod)
    kwota = round(masa * przelicznik, 2)
    return kwota

def quit():
    '''
    Funkcja służąca do zamykania programu

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    sys.exit()

def retrieve_waluta():
    '''
    Funkcja służąca do wyświetlania policzonej kwoty w innej walucie.

    Najpierw funkcja sprawdza czy podane argumenty są poprawne, jeżeli nie wyświetla odpowiedni komunikat.

    Jeżeli wszystko się zgadza liczy wartość przy użyciu funkcji przeliczanie i wyświetla wynik.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    uwaga_liczba.grid_forget()
    try:
        float(my_entry.get())
        kwota = float(my_entry.get())
        waluta1 = combo1.current()
        waluta2 = combo2.current()
        if waluta1 != -1 and waluta != -1:
            wartość = przeliczanie(kody_walut[waluta1], kody_walut[waluta2], kwota)
            wynik.config(text = f"{wartość}" + " " + f"{kody_walut[waluta2]}")
        else:
            uwaga_liczba.grid(column = 1, row =5, columnspan =3)
    except ValueError:
        my_entry.delete(0, END)
        uwaga_liczba.grid(column = 1, row =5, columnspan =3)

def retrieve_masa():
    '''
    Funkcja służąca do wyświetlania ceny masy złota w danej walucie.

    Najpierw funkcja sprawdza czy podane argumenty są poprawne, jeżeli nie wyświetla odpowiedni komunikat.

    Jeżeli wszystko się zgadza liczy wartość przy użyciu funkcji złoto w gramach i wyświetla wynik.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    uwaga_liczba.grid_forget()
    try:
        float(masa.get())
        ilość = float(masa.get())
        waluta = waluta_zloto.current()
        if waluta != -1:
            wartość = złoto_w_gramach(kody_walut[waluta], ilość)
        masa_wynik.config(text = f'{wartość}' + " " + f"{kody_walut[waluta]}")
    except ValueError:
        masa.delete(0, END)
        uwaga_liczba.grid(column = 1, row =5, columnspan =3)

def show_gold():
    '''
    Funkcja służąca do pokazywania opcji związanych ze złotem.

    Najpierw funkcja pokazuje wszystkie opcje związane ze złotem, a potem zmienia napis i działanie przycisku.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''
    
    cena_zloto.config(text = f"Cena złota za 1g wynosi: {kurs_złota}" + " PLN")
    cena_zloto.grid(column = 0, row = 4)
    waluta_zloto.grid(column = 1, row = 4, padx = 5, pady = 5)
    waluta_zloto_label.grid(column=1, row = 3, sticky=tk.S)
    masa_zloto_label.grid(column=2, row=3, sticky=tk.S)
    masa.grid(column = 2, row = 4, padx = 5, pady = 5)
    result.grid(column = 4, row = 3, sticky =tk.S)
    btn3.grid(column = 3, row = 4)
    dane_zloto_label.grid(column = 0, row = 5, sticky = tk.SW)
    zloto.config(text = "Ukryj cenę złota", command = hide_gold)

def hide_gold():
    '''
    Funkcja służąca do ukrywania opcji związanych ze złotem.

    Najpierw funkcja zmienia napis i działanie przycisku, a potem ukrywa wszystkie opcje związane ze złotem.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    zloto.config(text = "Wyświetl cenę złota w PLN", command = show_gold)
    cena_zloto.grid_forget()
    waluta_zloto.grid_forget()
    waluta_zloto_label.grid_forget()
    masa_zloto_label.grid_forget()
    masa.grid_forget()
    result.grid_forget()
    btn3.grid_forget()
    dane_zloto_label.grid_forget()

def handle_focus_in_waluta(_):
    '''
    Funkcja służąca do zmieniania koloru czcionki my_entry po kliknięciu na czarny.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    my_entry.delete(0, END)
    my_entry.config(fg='black')

def handle_focus_out_waluta(_):
    '''
    Funkcja służąca do zmieniania koloru czcionki, my_entry po odkliknięciu i gdy nie ma tam tekstu, na szary i pokazywaniu tam odpowiedniego tekstu.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    if my_entry.get() == "":
        my_entry.delete(0, END)
        my_entry.config(fg='grey')
        my_entry.insert(0, "Podaj tu liczbę")

def handle_focus_in_masa(_):
    '''
    Funkcja służąca do zmieniania koloru czcionki masa po kliknięciu na czarny.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    masa.delete(0, END)
    masa.config(fg='black')

def handle_focus_out_masa(_):
    '''
    Funkcja służąca do zmieniania koloru czcionki, masa po odkliknięciu i gdy nie ma tam tekstu, na szary i pokazywaniu tam odpowiedniego tekstu.

            Parametry:
                    Brak
            Zwrot:
                    Brak
    '''

    if masa.get() == "":
        masa.delete(0, END)
        masa.config(fg='grey')
        masa.insert(0, "Podaj tu liczbę")



root = Tk()
txt = "Kalkulator walutowy"
root.title(txt)
root.geometry('1100x300+400+200')

frame = Frame(root)
frame.grid()

root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=1)
root.columnconfigure(2, weight=1)
root.columnconfigure(3, weight=1)
root.columnconfigure(4, weight=1)

root.rowconfigure(0, weight = 1)
root.rowconfigure(1, weight = 1)
root.rowconfigure(2, weight = 1)
root.rowconfigure(3, weight = 5)
root.rowconfigure(4, weight = 1)
root.rowconfigure(5, weight = 4)
root.rowconfigure(5, weight = 6)

my_entry = Entry(fg="grey")
my_entry.insert(0, "")
my_entry.grid(column = 0, row = 1, padx = 5, pady = 5, sticky=tk.N)

combo1 = ttk.Combobox(values = pelne_nazwy, state="readonly", width=37)
combo1.grid(column = 1, row = 1, padx = 5, pady = 5, sticky=tk.N)

combo2 = ttk.Combobox(values = pelne_nazwy, state="readonly", width=37)
combo2.grid(column = 3, row = 1, padx = 5, pady = 5, sticky=tk.N)

btn = Button(root, text="Przelicz", command=retrieve_waluta, width = 15)
btn.grid(column = 2, row = 1, padx = 5, pady = 5, sticky=tk.N)

kwota_label = ttk.Label(root, text="Podaj kwotę")
kwota_label.grid(column=0, row=0, padx=5, pady=5, sticky=tk.N)

waluty1_label = ttk.Label(root, text="Z waluty")
waluty1_label.grid(column=1, row=0, padx=5, pady=5, sticky=tk.N)

waluty2_label = ttk.Label(root, text="Na walutę")
waluty2_label.grid(column=3, row=0, padx=5, pady=5, sticky=tk.N)

btn2 = Button(root, text="Koniec", command=quit)
btn2.grid(column = 2, row = 6, padx = 5, pady = 5, sticky=tk.S)

wynik = ttk.Label(root)
wynik.grid(column = 4, row = 1, sticky=tk.N)

wynik_label = ttk.Label(root, text="Kwota po przeliczeniu wynosi")
wynik_label.grid(column=4, row=0, padx=5, pady=5, sticky=tk.N)

zloto = Button(root, text = "Wyświetl cenę złota w PLN", command = show_gold, width=20)
zloto.grid(column = 0, row = 3, sticky=tk.S) 

cena_zloto = ttk.Label(root)
cena_zloto.grid(column = 0, row = 4)

waluta_zloto = ttk.Combobox(values = pelne_nazwy, state="readonly", width=37)
waluta_zloto.grid(column = 1, row = 4, padx = 5, pady = 5)

waluta_zloto_label = ttk.Label(root, text="W walucie")
waluta_zloto_label.grid(column=1, row = 3, sticky=tk.S)

masa_zloto_label = ttk.Label(root, text ="Masa (w g)")
masa_zloto_label.grid(column=2, row=3, sticky=tk.S)

masa = Entry(width = 15, fg = "grey")
masa.insert(0, "")
masa.grid(column = 2, row = 4, padx = 5, pady = 5)

result = ttk.Label(root, text="Cena tej ilości złota wynosi")
result.grid(column = 4, row = 3)

masa_wynik = ttk.Label(root)
masa_wynik.grid(column = 4, row = 4)

btn3 = Button(root, text="Przelicz", command=retrieve_masa)
btn3.grid(column = 3, row = 4)

dane_walut_label = ttk.Label(root, text = f"Dane dla walut na dzień: {data_walut}")
dane_walut_label.grid(column = 0, row = 5, sticky=tk.W)

dane_zloto_label = ttk.Label(root, text = f"Dane dla złota na dzień: {data_złota}")
dane_zloto_label.grid(column = 0, row = 5, sticky = tk.SW)
dane_zloto_label.grid_forget()

uwaga_liczba = tk.Label(root, text="Podaj poprawnie wszystkie dane!", fg='red', font = 40)
uwaga_liczba.grid(column = 1, row =5, columnspan =3)
uwaga_liczba.grid_forget()

if internet == False:
    internet_label = tk.Label(root, text = "Nie udało się pobrać danych z internetu, informacje mogą być nieaktualne", fg="grey")
    internet_label.grid(column =3, row=6, columnspan=2)

my_entry.insert(0, "Podaj tu liczbę")
my_entry.bind("<FocusIn>", handle_focus_in_waluta)
my_entry.bind("<FocusOut>", handle_focus_out_waluta)

masa.insert(0, "Podaj tu liczbę")
masa.bind("<FocusIn>", handle_focus_in_masa)
masa.bind("<FocusOut>", handle_focus_out_masa)

waluta_zloto.grid_forget()
waluta_zloto_label.grid_forget()
masa_zloto_label.grid_forget()
masa.grid_forget()
result.grid_forget()
btn3.grid_forget()

root.mainloop()