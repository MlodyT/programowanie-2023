import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import imageio as io
import os

def random_walk (steps, graph_type, gif_name):
    '''
    Funkcja tworzy gif z bładzeniem losowym agenta po grafie.

    Zależnie od podanego typu grafu, funkcja generuje odpowiedni graf, a następnie losuje pozycję początkową agenta.

    Później koloruje wszystkie punkty grafu na niebiesko, a ten w którym jest aktualnie agent na czerwono.

    Potem rysuje graf dla pierwszej pozycji i każdego z kolejnych kroków, za każdym rysując nowa pozycję agenta i kolorując odwiedzone już punkty na zielono.
    
    Wszystkie pozycje agenta zapisuje jako pliki, a następnie tworzy z nich gif zapisany pod podaną nazwą.

            Parametry:
                    steps (int): Ilość kroków agenta
                    graph_type (string): Wybrany rodzaj grafu
                    gif_name (string): Nazwa pod jaką ma zostać zapisany plik

            Zwrot:
                    Brak
    '''

    if graph_type == "random graph":
        graph = nx.Graph()
        graph = nx.gnp_random_graph(20, 0.18)
    elif graph_type == "Barabasi-Albert graph":
        graph = nx.Graph()
        graph = nx.barabasi_albert_graph(20, 3)
    elif graph_type == "Watts-Strogatz graph":
        graph = nx.Graph()
        graph = nx.watts_strogatz_graph(20, 4, 0.1)
    else:
        raise TypeError("Program nie obsługuje tego typu grafów. Obsługiwane typy to: random graph, Barabasi-Albert graph i Watts-Strogatz graph")
    
    pozycja = np.random.randint(0, 20, 1)[0]
    color_map = []

    if type(steps) == int and steps > 0:
        if gif_name.lower().endswith(('.gif')):
            for i in graph.nodes:
                color_map.append("blue")
            color_map[pozycja] = "red"

            pos = nx.spring_layout(graph, k = 0.15, iterations = 20)
            
            plt.title(f"Random walk on {graph_type} (n = {steps} steps)\nstep = 0")
            nx.draw (graph, pos, with_labels = True, node_size = 200, width = 0.5, font_size = 9, node_color = color_map)
            plt.savefig(fr"C:\Users\Dell\OneDrive\Pulpit\gif_images\images_for_gif_0.jpg")
            
            for i in range(steps):
                sąsiedzi = []
                for neighbor in graph.neighbors(pozycja):
                    sąsiedzi.append(neighbor)
                color_map[pozycja] = "green"
                pozycja = np.random.choice(sąsiedzi)
                color_map[pozycja] = "red"
                plt.title(f"Random walk on {graph_type} (n = {steps} steps) \nstep = {i + 1}")
                nx.draw (graph, pos, with_labels = True, node_size = 200, width = 0.5, font_size = 9, node_color = color_map)
                plt.savefig(fr"C:\Users\Dell\OneDrive\Pulpit\gif_images\images_for_gif_{i + 1}.jpg")
            
            path = r"C:\Users\Dell\OneDrive\Pulpit\gif_images"
            frames = []
            
            for i in range(steps + 1):
                image = io.v3.imread(fr"C:\Users\Dell\OneDrive\Pulpit\gif_images\images_for_gif_{i}.jpg")
                frames.append(image)
            io.mimsave(gif_name, frames, duration = 750, loop = 0)
        else:
            raise TypeError("Podana nazwa pliku nie jest formatu gif")
    else:
        raise TypeError("Podana ilość kroków nie jest liczbą naturalną")
        

random_walk(10, "Watts-Strogatz graph", r"agent3.gif")