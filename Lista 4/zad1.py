from scipy.integrate import odeint
import sys
import numpy as np
import matplotlib.pyplot as plt
import string

def equations(x, t, beta, sigma, gamma):
    '''
    Funkcja zawiera równania różniczkowe, które trzeba rozwiązać w modelu epidemii SEIR.

    Na początku parametrom S, E, I, R, beta, sigma i gamma przypisywane są podane wartości, a potem na ich podstawie funkcja liczy pochodne danych populacyjnych po czasie.

    Na końcu zwraca to co zostało policzone w liście.

            Parametry:
                    x (list): Lista z wartościami, które zostaną przypisane S, E, I i R
                    t (np.ndarray): Obiekt przechowujący wartości czasu
                    beta (float): Paramter potrzebny przy równaniach różniczkowych, wskaźnik infekcji
                    sigma (float): Paramter potrzebny przy równaniach różniczkowych, wskaźnik inkubacji
                    gamma (float): Paramter potrzebny przy równaniach różniczkowych, wskaźnik wyzdrowień

            Zwrot:
                    [dSdt, dEdt, dIdt, dRdt] (list): Lista z wartościami policzonych pocodnych po czasie
    '''

    S, E, I, R = x
    N = S + E + I + R           
    dSdt = -beta * S * I / N
    dEdt = beta * S * I / N - sigma * E
    dIdt = sigma * E - gamma * I
    dRdt =  gamma * I
    return [dSdt, dEdt, dIdt, dRdt]

def epidemic(days, data):
    '''
    Funkcja tworzy wykres przbiegu pandemii w zadanym okresie czasowym na podstawie zadanych danych.

    Na początku sprawdza czy ilość podanych paramterów się zgadza i czy wszystkie podane wartości to liczby, a także czy liczba ogólnej populacji N to suma parametrów S, E, I i R oraz czy wszystkie parametry są dodatnie.

    Gdy wszystko się zgadza, najpierw tworzy obiekt przechowujący liczby od 0 do wartości parametru days liczb, w równych odstępach wynoszących 1.

    Następnie przy pomocy funkcji odeint rozwiązuje nasze równania różniczkowe na zadanym przedziale czasowym.

    Później zapisuje wyliczone wartości w odpowiednich listach i na ich podstawie tworzy wykres

    Na końcu zapisuje stworzony wykres.

            Parametry:
                    days (int): Ilość dni, podczas, których będzie przeprowadzana nasz symulacja
                    data (list): Lista ze wszystkimi parametrami potrzebnymi do naszej symulacji

            Zwrot:
                    Brak
    '''

    if len(data) == 9:
        długość = 0
        liczby = 0
        kropka = 0
                
        for i in data[1:10]:
            długość = długość + len(i)
            for j in i:
                if j in string.digits:
                    liczby = liczby + 1
                elif j == ".":
                    kropka = kropka + 1
        
        if długość == liczby + kropka and kropka <= 3:
            for i in range(1, 6):
                data[i] = int(data[i])

            for i in range(6, 9):
                data[i] = float(data[i])

            if data[1] == data[2] + data[3] + data[4] + data[5]:
                if data[1] > 0 and data[2] >= 0 and data[3] >= 0 and data[4] >= 0 and data[5] >= 0 and data[6] > 0 and data[7] > 0 and data[8] > 0:
                    time = np.arange(0, days, 1)

                    rozwiązanie = odeint(equations, [data[2], data[3], data[4], data[5]], time, args = (data[6], data[7], data[8]))

                    S = []
                    E = []
                    I = []
                    R = []

                    for i in time:
                        S.append(rozwiązanie[i][0])
                        E.append(rozwiązanie[i][1])
                        I.append(rozwiązanie[i][2])
                        R.append(rozwiązanie[i][3])

                    plt.plot(time, S, label = "S")
                    plt.plot(time, E, label = "E")
                    plt.plot(time, I, label = "I")
                    plt.plot(time, R, label = "R")

                    plt.suptitle("Model pandemii SEIR", fontweight = "bold", fontsize = "x-large", fontfamily = "monospace")
                    plt.title("N=%s, S=%s, E=%s, I=%s, R=%s, beta=%s, sigma=%s, gamma=%s"%(data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]), fontstyle = "italic", fontsize = "small")
                    plt.xlabel("Dni")
                    plt.ylabel("Populacja")
                    plt.legend()

                    plt.grid(True)
                    plt.gca().spines["top"].set_alpha(0.0)    
                    plt.gca().spines["bottom"].set_alpha(0.2)
                    plt.gca().spines["right"].set_alpha(0.0)    
                    plt.gca().spines["left"].set_alpha(0.2)
                    plt.savefig("Wykres11.jpg")
                else:
                    raise ValueError("Parametry muszą być dodatnie")
            else:
                raise ValueError("Populacja N musi być równa suma parametrów S, E, I, R")
        else:
            raise ValueError("Podane parmatery muszą być liczbami - pięć pierwszych całkowitymi, trzy ostatnie dodatnimi")
    else:
        raise ValueError("Powinieneś podać dokładnie 8 argumentów")

dni = 80
dane = sys.argv

epidemic(dni, dane)