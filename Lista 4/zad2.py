from scipy.integrate import odeint
import sys
import numpy as np
import matplotlib.pyplot as plt
import string
import argparse

def equations(x, t, beta, sigma, gamma):
    '''
    Funkcja zawiera równania różniczkowe, które trzeba rozwiązać w modelu epidemii SEIR.

    Na początku parametrom S, E, I, R, beta, sigma i gamma przypisywane są podane wartości, a potem na ich podstawie funkcja liczy pochodne danych populacyjnych po czasie.

    Na końcu zwraca to co zostało policzone w liście.

            Parametry:
                    x (list): Lista z wartościami, które zostaną przypisane S, E, I i R
                    t (np.ndarray): Obiekt przechowujący wartości czasu
                    beta (float): Paramter potrzebny przy równaniach różniczkowych, wskaźnik infekcji
                    sigma (float): Paramter potrzebny przy równaniach różniczkowych, wskaźnik inkubacji
                    gamma (float): Paramter potrzebny przy równaniach różniczkowych, wskaźnik wyzdrowień

            Zwrot:
                    [dSdt, dEdt, dIdt, dRdt] (list): Lista z wartościami policzonych pocodnych po czasie
    '''

    S, E, I, R = x
    N = S + E + I + R           
    dSdt = -beta * S * I / N
    dEdt = beta * S * I / N - sigma * E
    dIdt = sigma * E - gamma * I
    dRdt =  gamma * I
    return [dSdt, dEdt, dIdt, dRdt]

def epidemic(days, data):
    '''
    Funkcja tworzy wykres przbiegu pandemii w zadanym okresie czasowym na podstawie zadanych danych.

    Na początku sprawdza czy liczba ogólnej populacji N to suma parametrów S, E, I i R.

    Gdy wszystko się zgadza, najpierw tworzy obiekt przechowujący liczby od 0 do wartości parametru days liczb, w równych odstępach wynoszących 1.

    Następnie przy pomocy funkcji odeint rozwiązuje nasze równania różniczkowe na zadanym przedziale czasowym.

    Później zapisuje wyliczone wartości w odpowiednich listach i na ich podstawie tworzy wykres

    Na końcu pokazuje stworzony wykres.

            Parametry:
                    days (int): Ilość dni, podczas, których będzie przeprowadzana nasz symulacja
                    data (list): Lista ze wszystkimi parametrami potrzebnymi do naszej symulacji

            Zwrot:
                    Brak
    '''

    if data[0] == data[1] + data[2] + data[3] + data[4]:
        time = np.arange(0, days, 1)

        rozwiązanie = odeint(equations, [data[1], data[2], data[3], data[4]], time, args = (data[5], data[6], data[7]))

        S = []
        E = []
        I = []
        R = []

        for i in time:
            S.append(rozwiązanie[i][0])
            E.append(rozwiązanie[i][1])
            I.append(rozwiązanie[i][2])
            R.append(rozwiązanie[i][3])

        plt.plot(time, S, label = "S")
        plt.plot(time, E, label = "E")
        plt.plot(time, I, label = "I")
        plt.plot(time, R, label = "R")

        plt.suptitle("Model pandemii SEIR", fontweight = "bold", fontsize = "x-large", fontfamily = "monospace")
        plt.title("N=%s, S=%s, E=%s, I=%s, R=%s, beta=%s, sigma=%s, gamma=%s"%(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]), fontstyle = "italic", fontsize = "small")
        plt.xlabel("Dni")
        plt.ylabel("Populacja")
        plt.legend()

        plt.grid(True)
        plt.gca().spines["top"].set_alpha(0.0)    
        plt.gca().spines["bottom"].set_alpha(0.2)
        plt.gca().spines["right"].set_alpha(0.0)    
        plt.gca().spines["left"].set_alpha(0.2)
        plt.show()
    else:
        raise ValueError("Populacja N musi być sumą parametrów S, E, I i R")

dni = 200

parser = argparse.ArgumentParser()

parser.add_argument('-N', type = int, default = 1000, help = "Ogólna populacja")
parser.add_argument('-S', type = int, default = 999, help = "Populacja, która może zachorować, ale nie jest ")
parser.add_argument('-E', type = int, default = 1, help = "Populacja aktualnie narażonych na zakażenie")
parser.add_argument('-I', type = int, default = 0, help = "Populacja chorych")
parser.add_argument('-R', type = int, default = 0, help = "Populacja wyzdrowiałych po chorobie")
parser.add_argument('-beta', type = float, default = 1.5, help = "Wskaźnik tempa rozprzestrzeniania się infekcji")
parser.add_argument('-sigma', type = float, default = 0.2, help = "Wskaźnik inkubacji chorby")
parser.add_argument('-gamma', type = float, default = 0.5, help = "Wskaźnik czasu trwania infekcji")

dictionary = vars(parser.parse_args())
dane = []

for i in dictionary:
    dane.append(dictionary[i])

epidemic(dni, dane)